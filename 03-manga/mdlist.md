---
label: MDLists
order: 90
icon: list-unordered
---

# Add Manga to Lists

!!!info
This section requires [authentication](/02-authentication/index.md).
!!!

You can add Manga to:

- Your Reading List to keep track of your reading library
- Your Follows List to receive updates about new chapters
- Your own Custom Lists to group Manga that you find similar in any form

## Adding Manga to a Reading List

```http
 POST /manga/{id}/status
```

There are six kinds of reading lists, each with a straightforward meaning:

- Reading
- On Hold
- Dropped
- Plan to Read
- Completed
- Re-Reading

##### Request

Let's add the manga [Ichijou-San Wa Kao Ni Deyasui](https://mangadex.org/title/bbdaa3a3-ea49-4f12-9e1c-baa452f0830d/) to
a reading list as "reading."

+++Python

:::code-block

```python
manga_id = "bbdaa3a3-ea49-4f12-9e1c-baa452f0830d"
status = "reading"
```

:::

:::code-block

```python
session_token = "somesessiontoken"
```

:::

:::code-block

```python
import requests

base_url = "https://api.mangadex.org"

r = requests.post(
    f"{base_url}/manga/{manga_id}/status",
    headers={
        "Authorization": f"Bearer {session_token}"
    },
    json={"status": status},
)

print(r.json()["result"])
```

:::

+++JavaScript

:::code-block

```javascript
const mangaID = 'bbdaa3a3-ea49-4f12-9e1c-baa452f0830d';
const status = 'reading';
```

:::

:::code-block

```javascript
const sessionToken = 'somesessiontoken';
```

:::

:::code-block

```javascript
const axios = require('axios');

const baseUrl = 'https://api.mangadex.org';

const resp = await axios({
    method: 'POST',
    url: `${baseUrl}/manga/${mangaID}/status`,
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${sessionToken}`
    },
    data: {
        status: status
    }
});

console.log(resp.data.result);

```

:::

+++

!!!
To remove a Manga from a Reading List, pass a `null` value on the `status` field instead of one of the enums.
!!!

## Adding Manga to the Follows List

```http
 POST /manga/{id}/follow
```

To receive updates on your feed when a new chapter is uploaded, you need to add the Manga to your Follows List.

##### Request

Let's add the manga [Alma-Chan Wants to Be a Family](https://mangadex.org/title/a37d2a4a-6caa-4ff3-84fe-f137f97b207c/)
to our Follows List.

!!!info
Since we don't pass a body for this endpoint, we can omit the Content-Type header.
!!!

+++Python

:::code-block

```python
manga_id = "a37d2a4a-6caa-4ff3-84fe-f137f97b207c"
```

:::

:::code-block

```python
session_token = "somesessiontoken"
```

:::

:::code-block

```python
import requests

base_url = "https://api.mangadex.org"

r = requests.post(
    f"{base_url}/manga/{manga_id}/follow",
    headers={
        "Authorization": f"Bearer {session_token}"
    },
)

print(r.json()["result"])
```

:::

+++JavaScript

:::code-block

```javascript
const mangaID = 'a37d2a4a-6caa-4ff3-84fe-f137f97b207c';
```

:::

:::code-block

```javascript
const sessionToken = 'somesessiontoken';
```

:::

:::code-block

```javascript
const axios = require('axios');

const baseUrl = 'https://api.mangadex.org';

const resp = await axios({
    method: 'POST',
    url: `${baseUrl}/manga/${mangaID}/follow`,
    headers: {
        'Authorization': `Bearer ${sessionToken}`
    }
});

console.log(resp.data.result);

```

:::

+++

## Adding Manga to a Custom List

To add a Manga into our Custom List, we first need to create that List.

### Creating and populating the Custom List

```http
 POST /list
```

To create our Custom List, we need to decide on a name, and its visibility.

##### Request

Our Custom List's name shall be "Hidden Gems" and it shall be visible to the public.

!!!info
You may add Manga to the List just as you create it, by providing an array of Manga IDs in the `manga` field.
!!!

+++Python

:::code-block

```python
options = {
    "name": "Hidden Gems",
    "visibility": "public",
}
```

:::

:::code-block

```python
session_token = "somesessiontoken"
```

:::

:::code-block

```python
import requests

base_url = "https://api.mangadex.org"

r = requests.post(
    f"{base_url}/list",
    headers={
        "Authorization": f"Bearer {session_token}"
    },
    json=options,
)

print(
    "List created with ID:",
    r.json()["data"]["id"],
)
```

:::

+++JavaScript

:::code-block

```javascript
const options = {
    name: 'Hidden Gems',
    visibility: 'public'
};
```

:::

:::code-block

```javascript
const sessionToken = 'somesessiontoken';
```

:::

:::code-block

```javascript
const axios = require('axios');

const baseUrl = 'https://api.mangadex.org';

const resp = await axios({
    method: 'POST',
    url: `${baseUrl}/list`,
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${sessionToken}`
    },
    data: options
});

console.log('List created with ID:', resp.data.data.id);

```

:::

+++

### Updating the Manga in the Custom List

```http
 PUT /list/{id}
```

Now that our Custom List has been created, and we have its ID, we can start updating what Manga it contains.

!!!info
There aren't endpoints for adding or removing specific Manga to a Custom List. We will be managing that locally.
!!!

##### Request

+++Python

We start by fetching the List and storing the Manga it contains.

:::code-block

```python
list_id = "e1d40cf9-33e9-4e80-a64c-7354d4c520d3"
```

:::

:::code-block

```python
session_token = "somesessiontoken"
```

:::

:::code-block

```python
import requests

base_url = "https://api.mangadex.org"

r = requests.get(
    f"{base_url}/list/{list_id}",
    headers={
        "Authorization": f"Bearer {session_token}"
    },
)

manga_ids = [
    relationship["id"]
    for relationship in r.json()["data"]["relationships"]
    if relationship["type"] == "manga"
]

version = r.json()["data"]["attributes"]["version"]
```

:::

!!!warning
We **have** to store the version sent to us if we want to update the List. This is to avoid cache issues with updating
an outdated version of the resource.
!!!

Then, we update the List locally.

:::code-block

```python
manga_ids_to_add = [
    "bbdaa3a3-ea49-4f12-9e1c-baa452f0830d",
    "8c21fe3b-4fe6-4f11-b51b-ced00d8aec60",
]

manga_ids_to_remove = [
    "719f4514-76c1-4efd-b7a1-331fa1e42eb6"
]

new_manga_ids = [
                    manga
                    for manga in manga_ids
                    if manga
                       not in (manga_ids_to_add + manga_ids_to_remove)
                ] + manga_ids_to_add

```

:::

Finally, we send the request to update the List.

:::code-block

```python
r = requests.put(
    f"{base_url}/list/{list_id}",
    headers={
        "Authorization": f"Bearer {session_token}"
    },
    json={
        "manga": new_manga_ids,
        "version": version,
    },
)

print(r.json()["result"])

```

:::

+++JavaScript

We start by fetching the List and storing the Manga it contains.

:::code-block

```javascript
const listID = 'e1d40cf9-33e9-4e80-a64c-7354d4c520d3'
```

:::

:::code-block

```javascript
const sessionToken = 'somesessiontoken';
```

:::

:::code-block

```javascript
const axios = require('axios');

const baseUrl = 'https://api.mangadex.org';

const resp = await axios({
    method: 'GET',
    url: `${baseUrl}/list/${listID}`,
    headers: {
        'Authorization': `Bearer ${sessionToken}`
    }
});

const mangaIDs = resp.data.data.relationships
    .filter(item => item.type === 'manga')
    .map(manga => manga.id);

const version = resp.data.data.attributes.version;

```

:::

!!!warning
We **have** to store the version sent to us if we want to update the List. This is to avoid cache issues with updating
an outdated version of the resource.
!!!

Then, we update the List locally.

:::code-block

```javascript
const mangaIDsToAdd = [
    'bbdaa3a3-ea49-4f12-9e1c-baa452f0830d',
    '8c21fe3b-4fe6-4f11-b51b-ced00d8aec60'
];

const mangaIDsToRemove = [
    '719f4514-76c1-4efd-b7a1-331fa1e42eb6'
];

const newMangaIDs = mangaIDs.filter(manga => !mangaIDsToAdd
    .concat(mangaIDsToRemove)
    .includes(manga)
).concat(mangaIDsToAdd);
```

:::

Finally, we send the request to update the List.

:::code-block

```javascript
const resp = await axios({
    method: 'PUT',
    url: `${baseUrl}/list/${listID}`,
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${sessionToken}`
    },
    data: {
        manga: newMangaIDs,
        version: version
    }
});

console.log(resp.data.result);

```

:::

+++
