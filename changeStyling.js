// Quick script to change the RGB values of the css styling
// retype has chose. They don't support theming and even after
// a user has asked along with a showcase of how it'd be like,
// https://github.com/retypeapp/retype/discussions/282
// they're still adamant that it's "too complicated,"
// even though they charge premium for their service.

const fs = require('fs');

const folder = process.argv[2];

fs.readdirSync(`./${folder}/resources/css`).forEach(file => {
    if (!file.startsWith('retype')) return;

    const map = {
        "225 237 255": "255 225 217",
        "179 210 255": "255 190 173",
        "141 187 255": "254 168 146",
        "95 160 255": "255 103 64",
        "66 132 251": "230 97 62",
        "31 122 255": "188 85 59",
        "0 103 252": "142 43 18",
        "0 90 221": "99 27 9",
        "0 75 183": "56 14 3"
    };

    let styles = fs.readFileSync(`./${folder}/resources/css/${file}`).toString();

    Object.keys(map).forEach(color => {
        styles = styles.replaceAll(color, map[color]);
    });

    fs.writeFileSync(`${folder}/resources/css/${file}`, styles);
});
